# SQL

> Manipulations des bases données à travers des exercices.  

## Le language SQL : Savoir communiquer avec une base de données

Pour la réalisation des exercices il faudra au préalable :  

* Installation de Microsoft® SQL Server® 2014 Express
* Installation de SQL Management Studio

1. [BASE DE DONNEES : Création/Modification](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-1 "Lien")
2. [TABLE : Création](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-2 "Lien")
3. [TABLE : Modification](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-3 "Lien")
4. [DONNEE : Insertion](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-4 "Lien")
5. [DONNEE : Sélection](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-5 "Lien")
6. [DONNEE : Sélection élargie](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-6 "Lien")
7. [DONNEE : Modification](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-7 "Lien")
8. [TABLE : Jointure](https://gitlab.ecole-e2n.fr/AnousoneM/dot-net/tree/master/partie-3-web-app/sql/SQL-Exo-8 "Lien")
